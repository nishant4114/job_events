class ChangeTypeJob < ActiveRecord::Migration
  def change
    change_column :events, :type_job, :string
    add_column :events, :job_classify, :string
  end
end
