class CreateIndividualPhotos < ActiveRecord::Migration
  def change
    create_table :individual_photos do |t|
     t.belongs_to :profile, :index => true
     t.attachment :portfolio_photo
      t.timestamps null: false
    end
  end
end
