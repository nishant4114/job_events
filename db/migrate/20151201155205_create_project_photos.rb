class CreateProjectPhotos < ActiveRecord::Migration
  def change
    create_table :project_photos do |t|
      t.string :title
      t.attachment :project_photo
      t.timestamps null: false
    end
  end
end
