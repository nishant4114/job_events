class CreateRequestEvents < ActiveRecord::Migration
  def change
    create_table :request_events do |t|
      t.belongs_to :event, index: true
      t.belongs_to :individual, index: true
      t.timestamps null: false

    end
  end
end
