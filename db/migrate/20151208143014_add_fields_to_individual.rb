class AddFieldsToIndividual < ActiveRecord::Migration
  def change
    add_column :profiles, :looking_job, :string
    add_column :profiles, :type_job, :string
    add_column :profiles, :weekday_availability, :string
    add_column :profiles, :weekend_availability, :string
    add_column :profiles, :work_experience, :string
    add_column :profiles, :education, :string
    add_column :profiles, :month_salary, :string
    add_column :profiles, :hr_salary, :string
    add_column :profiles, :annum_salary, :string
    add_column :profiles, :project_salary, :string
    add_column :profiles, :area_interest, :string
    add_column :profiles, :key_skills, :string
    add_column :profiles, :current_city, :string
    add_column :profiles, :preferred_city, :string
  end
end
