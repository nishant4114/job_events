class AddContactColumnToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :contact_no, :string
    add_column :company_profiles, :contact_no, :string
  end
end
