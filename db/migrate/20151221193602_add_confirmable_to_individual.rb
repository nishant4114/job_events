class AddConfirmableToIndividual < ActiveRecord::Migration
  def change
    add_column :individuals, :confirmation_token, :string
    add_column :individuals, :confirmed_at, :datetime
    add_column :individuals, :confirmation_sent_at, :datetime
    add_column :individuals, :unconfirmed_email, :string
  end
end
