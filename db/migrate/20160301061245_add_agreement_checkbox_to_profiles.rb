class AddAgreementCheckboxToProfiles < ActiveRecord::Migration
  def change
    add_column :profiles, :agree_info, :boolean, :default => true
    add_column :company_profiles, :agree_info, :boolean, :default => true
  end
end
