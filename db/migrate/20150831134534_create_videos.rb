class CreateVideos < ActiveRecord::Migration
  def change
    create_table :videos do |t|
      t.string :link, null: false
      t.string :uid
      t.belongs_to :event, index: true
      t.belongs_to :company, index: true
      t.belongs_to :profile, index: true
      t.string :title
      t.string :author
      t.string :duration
      t.integer :likes
      t.integer :dislikes

      t.timestamps null: false
    end
  end
end
