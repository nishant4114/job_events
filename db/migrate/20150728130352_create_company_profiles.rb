class CreateCompanyProfiles < ActiveRecord::Migration
  def change
    create_table :company_profiles do |t|
      t.belongs_to :company, index: true
      t.string :company_name
      t.datetime :formed_date
      t.string :age
      t.string :type_company
      t.string :category
      t.string :bio
      t.string :linkedin_profile
      t.string :facebook_profile
      t.string :twitter_profile
      t.string :mobile_no
      t.string :address1
      t.string :address2
      t.string :city
      t.string :state
      t.string :country
      t.string :pincode
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
  end
end
