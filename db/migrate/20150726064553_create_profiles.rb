class CreateProfiles < ActiveRecord::Migration
  def change
    create_table :profiles do |t|
      t.belongs_to :individual, index: true
      t.string :first_name
      t.string :last_name
      t.string :age
      t.string :birth_date
      t.string :gender
      t.string :bio
      t.string :linkedin_profile
      t.string :facebook_profile
      t.string :twitter_profile
      t.string :mobile_no
      t.belongs_to :category, index: true
      t.timestamps null: false
    end
  end
end
