class AddCompanyIdToRequestEvents < ActiveRecord::Migration
  def change
    add_reference :request_events, :company, index: true
  end
end
