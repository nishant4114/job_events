class AddOmniauthToIndividuals < ActiveRecord::Migration
  def change
    add_column :individuals, :provider, :string
    add_column :individuals, :uid, :string
  end
end
