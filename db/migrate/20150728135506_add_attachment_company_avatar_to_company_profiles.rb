class AddAttachmentCompanyAvatarToCompanyProfiles < ActiveRecord::Migration
  def self.up
    change_table :company_profiles do |t|
      t.attachment :company_avatar
    end
  end

  def self.down
    remove_attachment :company_profiles, :company_avatar
  end
end
