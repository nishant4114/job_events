class CreateProjectUrls < ActiveRecord::Migration
  def change
    create_table :project_urls do |t|
      t.belongs_to :project
      t.string :title
      t.string :url
      t.timestamps null: false
    end
  end
end
