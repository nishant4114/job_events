class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :name
      t.attachment :document
      t.belongs_to :profile
      t.timestamps null: false
    end
  end
end
