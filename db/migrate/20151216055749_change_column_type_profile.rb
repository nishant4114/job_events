class ChangeColumnTypeProfile < ActiveRecord::Migration
  def change
    add_column :profiles, :individual_type, :string
  end
end
