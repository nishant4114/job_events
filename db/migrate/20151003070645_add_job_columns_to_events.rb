class AddJobColumnsToEvents < ActiveRecord::Migration
  def change
    add_column :events, :is_job, :boolean, default: true
    add_column :events, :person_req, :integer
    add_column :events, :salary, :string
    add_column :events, :type_job, :integer, default: 0
    add_column :events, :education, :string
    add_column :events, :experience, :string
    add_column :events, :gender, :string
    add_column :events, :job_status, :integer, default: 0

  end
end
