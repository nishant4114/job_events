class AddSponsorCountToEvents < ActiveRecord::Migration
  def change
    add_column :events, :sponsor_score, :integer, default: 0
  end
end
