class CreateEvents < ActiveRecord::Migration
  def change
    create_table :events do |t|
      t.references :company,        index: true
      t.string :name
      t.date :event_date
      t.time :event_time
      t.text :event_info
      t.string :place
      t.float :lattitude
      t.float :longitude
      t.string :find_name
      t.string :venue_name
      t.string :sub_locality
      t.string :locality
      t.string :state
      t.string :country
      t.string :country_code
      t.string :postal_code
      t.string :full_address
      t.text :interests
      t.belongs_to :category, index: true

      t.timestamps null: false
    end
  end
end
