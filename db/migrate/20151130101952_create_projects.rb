class CreateProjects < ActiveRecord::Migration
  def change
    create_table :projects do |t|
      t.belongs_to :profile, index: true
      t.belongs_to :company_profile, index: true
      t.belongs_to :category , index: true
      t.string :name
      t.integer :type_project
      t.string :description
      t.integer :sales_count
      t.string :price
      t.string :preview_url
      t.string :tags
      t.boolean :is_verified
      t.timestamps null: false
    end
  end
end
