# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160301061245) do

  create_table "activities", force: :cascade do |t|
    t.integer  "trackable_id",   limit: 4
    t.string   "trackable_type", limit: 255
    t.integer  "owner_id",       limit: 4
    t.string   "owner_type",     limit: 255
    t.string   "key",            limit: 255
    t.text     "parameters",     limit: 65535
    t.integer  "recipient_id",   limit: 4
    t.string   "recipient_type", limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "activities", ["owner_id", "owner_type"], name: "index_activities_on_owner_id_and_owner_type", using: :btree
  add_index "activities", ["recipient_id", "recipient_type"], name: "index_activities_on_recipient_id_and_recipient_type", using: :btree
  add_index "activities", ["trackable_id", "trackable_type"], name: "index_activities_on_trackable_id_and_trackable_type", using: :btree

  create_table "categories", force: :cascade do |t|
    t.string   "name",       limit: 255
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "companies", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
  end

  add_index "companies", ["email"], name: "index_companies_on_email", unique: true, using: :btree
  add_index "companies", ["reset_password_token"], name: "index_companies_on_reset_password_token", unique: true, using: :btree

  create_table "company_profiles", force: :cascade do |t|
    t.integer  "company_id",                  limit: 4
    t.string   "company_name",                limit: 255
    t.datetime "formed_date"
    t.string   "age",                         limit: 255
    t.string   "type_company",                limit: 255
    t.string   "category",                    limit: 255
    t.string   "bio",                         limit: 255
    t.string   "linkedin_profile",            limit: 255
    t.string   "facebook_profile",            limit: 255
    t.string   "twitter_profile",             limit: 255
    t.integer  "mobile_no",                   limit: 4
    t.string   "address1",                    limit: 255
    t.string   "address2",                    limit: 255
    t.string   "city",                        limit: 255
    t.string   "state",                       limit: 255
    t.string   "country",                     limit: 255
    t.string   "pincode",                     limit: 255
    t.integer  "category_id",                 limit: 4
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "company_avatar_file_name",    limit: 255
    t.string   "company_avatar_content_type", limit: 255
    t.integer  "company_avatar_file_size",    limit: 4
    t.datetime "company_avatar_updated_at"
    t.string   "contact_no",                  limit: 255
    t.boolean  "agree_info",                              default: true
  end

  add_index "company_profiles", ["category_id"], name: "index_company_profiles_on_category_id", using: :btree
  add_index "company_profiles", ["company_id"], name: "index_company_profiles_on_company_id", using: :btree

  create_table "documents", force: :cascade do |t|
    t.string   "name",                  limit: 255
    t.string   "document_file_name",    limit: 255
    t.string   "document_content_type", limit: 255
    t.integer  "document_file_size",    limit: 4
    t.datetime "document_updated_at"
    t.integer  "profile_id",            limit: 4
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
  end

  create_table "events", force: :cascade do |t|
    t.integer  "company_id",                limit: 4
    t.string   "name",                      limit: 255
    t.date     "event_date"
    t.time     "event_time"
    t.text     "event_info",                limit: 65535
    t.string   "place",                     limit: 255
    t.float    "lattitude",                 limit: 24
    t.float    "longitude",                 limit: 24
    t.string   "find_name",                 limit: 255
    t.string   "venue_name",                limit: 255
    t.string   "sub_locality",              limit: 255
    t.string   "locality",                  limit: 255
    t.string   "state",                     limit: 255
    t.string   "country",                   limit: 255
    t.string   "country_code",              limit: 255
    t.string   "postal_code",               limit: 255
    t.string   "full_address",              limit: 255
    t.text     "interests",                 limit: 65535
    t.integer  "category_id",               limit: 4
    t.datetime "created_at",                                             null: false
    t.datetime "updated_at",                                             null: false
    t.string   "event_avatar_file_name",    limit: 255
    t.string   "event_avatar_content_type", limit: 255
    t.integer  "event_avatar_file_size",    limit: 4
    t.datetime "event_avatar_updated_at"
    t.boolean  "is_job",                                  default: true
    t.integer  "person_req",                limit: 4
    t.string   "salary",                    limit: 255
    t.string   "type_job",                  limit: 255,   default: "0"
    t.string   "education",                 limit: 255
    t.string   "experience",                limit: 255
    t.string   "gender",                    limit: 255
    t.integer  "job_status",                limit: 4,     default: 0
    t.integer  "sponsor_score",             limit: 4,     default: 0
    t.string   "job_classify",              limit: 255
  end

  add_index "events", ["category_id"], name: "index_events_on_category_id", using: :btree
  add_index "events", ["company_id"], name: "index_events_on_company_id", using: :btree

  create_table "follows", force: :cascade do |t|
    t.integer  "followable_id",   limit: 4,                   null: false
    t.string   "followable_type", limit: 255,                 null: false
    t.integer  "follower_id",     limit: 4,                   null: false
    t.string   "follower_type",   limit: 255,                 null: false
    t.boolean  "blocked",                     default: false, null: false
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "follows", ["followable_id", "followable_type"], name: "fk_followables", using: :btree
  add_index "follows", ["follower_id", "follower_type"], name: "fk_follows", using: :btree

  create_table "individual_photos", force: :cascade do |t|
    t.integer  "profile_id",                   limit: 4
    t.string   "portfolio_photo_file_name",    limit: 255
    t.string   "portfolio_photo_content_type", limit: 255
    t.integer  "portfolio_photo_file_size",    limit: 4
    t.datetime "portfolio_photo_updated_at"
    t.datetime "created_at",                               null: false
    t.datetime "updated_at",                               null: false
  end

  add_index "individual_photos", ["profile_id"], name: "index_individual_photos_on_profile_id", using: :btree

  create_table "individuals", force: :cascade do |t|
    t.string   "email",                  limit: 255, default: "", null: false
    t.string   "encrypted_password",     limit: 255, default: "", null: false
    t.string   "reset_password_token",   limit: 255
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          limit: 4,   default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip",     limit: 255
    t.string   "last_sign_in_ip",        limit: 255
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "provider",               limit: 255
    t.string   "uid",                    limit: 255
    t.string   "confirmation_token",     limit: 255
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email",      limit: 255
  end

  add_index "individuals", ["email"], name: "index_individuals_on_email", unique: true, using: :btree
  add_index "individuals", ["reset_password_token"], name: "index_individuals_on_reset_password_token", unique: true, using: :btree

  create_table "profiles", force: :cascade do |t|
    t.integer  "individual_id",        limit: 4
    t.string   "first_name",           limit: 255
    t.string   "last_name",            limit: 255
    t.string   "age",                  limit: 255
    t.string   "birth_date",           limit: 255
    t.string   "gender",               limit: 255
    t.string   "bio",                  limit: 255
    t.string   "linkedin_profile",     limit: 255
    t.string   "facebook_profile",     limit: 255
    t.string   "twitter_profile",      limit: 255
    t.integer  "mobile_no",            limit: 4
    t.integer  "category_id",          limit: 4
    t.datetime "created_at",                                      null: false
    t.datetime "updated_at",                                      null: false
    t.string   "avatar_file_name",     limit: 255
    t.string   "avatar_content_type",  limit: 255
    t.integer  "avatar_file_size",     limit: 4
    t.datetime "avatar_updated_at"
    t.string   "looking_job",          limit: 255
    t.string   "type_job",             limit: 255
    t.string   "weekday_availability", limit: 255
    t.string   "weekend_availability", limit: 255
    t.string   "work_experience",      limit: 255
    t.string   "education",            limit: 255
    t.string   "month_salary",         limit: 255
    t.string   "hr_salary",            limit: 255
    t.string   "area_interest",        limit: 255
    t.string   "key_skills",           limit: 255
    t.string   "individual_type",      limit: 255
    t.string   "description",          limit: 255
    t.string   "contact_no",           limit: 255
    t.boolean  "agree_info",                       default: true
  end

  add_index "profiles", ["category_id"], name: "index_profiles_on_category_id", using: :btree
  add_index "profiles", ["individual_id"], name: "index_profiles_on_individual_id", using: :btree

  create_table "project_photos", force: :cascade do |t|
    t.string   "title",                      limit: 255
    t.string   "project_photo_file_name",    limit: 255
    t.string   "project_photo_content_type", limit: 255
    t.integer  "project_photo_file_size",    limit: 4
    t.datetime "project_photo_updated_at"
    t.datetime "created_at",                             null: false
    t.datetime "updated_at",                             null: false
  end

  create_table "project_urls", force: :cascade do |t|
    t.integer  "project_id", limit: 4
    t.string   "title",      limit: 255
    t.string   "url",        limit: 255
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  create_table "projects", force: :cascade do |t|
    t.integer  "profile_id",         limit: 4
    t.integer  "company_profile_id", limit: 4
    t.integer  "category_id",        limit: 4
    t.string   "name",               limit: 255
    t.integer  "type_project",       limit: 4
    t.string   "description",        limit: 255
    t.integer  "sales_count",        limit: 4
    t.string   "price",              limit: 255
    t.string   "preview_url",        limit: 255
    t.string   "tags",               limit: 255
    t.boolean  "is_verified"
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "projects", ["category_id"], name: "index_projects_on_category_id", using: :btree
  add_index "projects", ["company_profile_id"], name: "index_projects_on_company_profile_id", using: :btree
  add_index "projects", ["profile_id"], name: "index_projects_on_profile_id", using: :btree

  create_table "queries", force: :cascade do |t|
    t.string   "phone",      limit: 255
    t.string   "email",      limit: 255
    t.text     "query",      limit: 65535
    t.datetime "created_at",               null: false
    t.datetime "updated_at",               null: false
  end

  create_table "request_events", force: :cascade do |t|
    t.integer  "event_id",      limit: 4
    t.integer  "individual_id", limit: 4
    t.datetime "created_at",                          null: false
    t.datetime "updated_at",                          null: false
    t.integer  "status",        limit: 4, default: 0
    t.integer  "company_id",    limit: 4
  end

  add_index "request_events", ["company_id"], name: "index_request_events_on_company_id", using: :btree
  add_index "request_events", ["event_id"], name: "index_request_events_on_event_id", using: :btree
  add_index "request_events", ["individual_id"], name: "index_request_events_on_individual_id", using: :btree

  create_table "taggings", force: :cascade do |t|
    t.integer  "tag_id",        limit: 4
    t.integer  "taggable_id",   limit: 4
    t.string   "taggable_type", limit: 255
    t.integer  "tagger_id",     limit: 4
    t.string   "tagger_type",   limit: 255
    t.string   "context",       limit: 128
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id", "taggable_id", "taggable_type", "context", "tagger_id", "tagger_type"], name: "taggings_idx", unique: true, using: :btree
  add_index "taggings", ["taggable_id", "taggable_type", "context"], name: "index_taggings_on_taggable_id_and_taggable_type_and_context", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string  "name",           limit: 255
    t.integer "taggings_count", limit: 4,   default: 0
  end

  add_index "tags", ["name"], name: "index_tags_on_name", unique: true, using: :btree

  create_table "videos", force: :cascade do |t|
    t.string   "link",       limit: 255, null: false
    t.string   "uid",        limit: 255
    t.integer  "event_id",   limit: 4
    t.integer  "company_id", limit: 4
    t.integer  "profile_id", limit: 4
    t.string   "title",      limit: 255
    t.string   "author",     limit: 255
    t.string   "duration",   limit: 255
    t.integer  "likes",      limit: 4
    t.integer  "dislikes",   limit: 4
    t.datetime "created_at",             null: false
    t.datetime "updated_at",             null: false
  end

  add_index "videos", ["company_id"], name: "index_videos_on_company_id", using: :btree
  add_index "videos", ["event_id"], name: "index_videos_on_event_id", using: :btree
  add_index "videos", ["profile_id"], name: "index_videos_on_profile_id", using: :btree

end
