class SeekersController < ApplicationController
  def index
    params[:filterrific][:with_category_id] = params[:with_category_id] unless params[:filterrific].nil?
    params[:filterrific][:with_type] = params[:with_type] unless params[:filterrific].nil?

    @filterrific = initialize_filterrific(
        Profile,
        params[:filterrific],
        :select_options => {
            sorted_by: Profile.options_for_sorted_by,
            with_category_id: Category.options_for_select

        }
    ) or return
    if params[:tag]
      @seekers = @filterrific.find.paginate(page: params[:page], per_page: 6).tagged_with(params[:tag])
    else
      @seekers = @filterrific.find.paginate(page: params[:page], per_page: 6)
    end
    p @seekers
    p params
    p params[:filterrific]
    respond_to do |format|
      format.html
      format.js
    end

  end
end
