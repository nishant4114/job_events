class XviumerProfilesController < ApplicationController

  def index
    @profiles = Profile.all
    @categories = Category.all
  end

  def job_approval
    @jobs = Event.where(job_status: 0)
  end

  def job_approve
    p params
    @job = Event.where(id: params[:event_id]).first
    p @job
    if @job.update_attributes(:job_status => 1)
      respond_to do |format|
        format.html { redirect_to job_approval_xviumer_profiles_path, notice: 'the job has been approved' }
        format.js { render :layout => false }
      end
    else
      respond_to do |format|
        format.html { redirect_to job_approval_xviumer_profiles_path, alert: find_error(@job) }
        format.js { render :layout => false }
      end
    end
  end
end
