class ProfilesController < ApplicationController
  protect_from_forgery except: :resume_upload
  before_action :authenticate_individual!, except: [:show, :follow_individual, :unfollow_individual]
  before_action :find_profile, only: [:show, :edit, :update, :destroy, :follow_individual, :unfollow_individual, :update_profile]
  # before_action :redirect_to_profile_create, :except => [:new, :create, :destroy, :autocomplete_tag_name]
  autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag', :full => true

   # skip_before_action :redirect_to_profile_create, :only => [:new, :create, :destroy]
  # autocomplete :tag, :name, :class_name => 'ActsAsTaggableOn::Tag'

  def index
    @profiles = Profile.all
  end

  def new
    @profile = current_individual.build_profile
    @categories = Category.all
    p @categories
  end

  def create

    @categories = Category.all
    p params

    number_to_send_to = params[:profile][:contact_no]
    p number_to_send_to

    twilio_sid = "AC49d1afb324109edf5638a687ea7c2dcc"
    twilio_token = "a87772c81432ac1dfa07528ce1f003c4"
    twilio_phone_number = "719-428-4401"
    @profile = current_individual.build_profile(profile_params)
    if @profile.save
      # begin
      #   @twilio_client = Twilio::REST::Client.new twilio_sid, twilio_token
      #
      #   @twilio_client.account.sms.messages.create(
      #       :from => "+1#{twilio_phone_number}",
      #       :to => "+91#{number_to_send_to}",
      #       :body => "Welcome to the world of Lets Login, Your Profile has been successfully created."
      #   )
      # rescue Twilio::REST::RequestError => e
      #   puts e.message
      # end
      flash[:success] = "Profile saved"
      redirect_to  individual_profile_path(current_individual)
    else
      flash[:error] = find_error(@profile)
      render :new
    end
  end

  def update

    p params
    p 'hello'

    if @profile.update(avatar_params)
    @message = 'Avatar has successfully been uplaoded'
    respond_to do |format|
      format.html { redirect_to individual_profile_path(current_individual), notice: 'Avatar successfully updated.' }
      format.js { render :layout => false }
    end
    else
      respond_to do |format|
        format.html { redirect_to individual_profile_path(current_individual), alert: find_error(@profile) }
        format.js { render :layout => false }
      end
    @message = find_error(@profile)
      p @message
    end

    # redirect_to  individual_profile_path(current_individual)
  end

  def edit
   @categories = Category.all
  end

  def update_profile
    @categories = Category.all
    if @profile.update(profile_params)
      @message = 'Your profile has successfully been updated'
      respond_to do |format|
        format.html { redirect_to individual_profile_path(current_individual), notice: 'Your profile has successfully been updated' }
        format.js { render :layout => false }
      end
    else
      respond_to do |format|
        format.html { redirect_to edit_profile_path(@profile), alert: find_error(@profile) }
        format.js { render :layout => false }
      end
      @message = find_error(@profile)
      p @message
    end
  end

  def show
    p 'popopopoppop'
    p @profile.id
    p params
    p params[:id]
    p @profile.id
    @portfolio_photo = IndividualPhoto.new
    @profile_photos = IndividualPhoto.where(profile_id: @profile.id)
    p ' aaaaaaa'
    p @profile.individual_photos
    if company_signed_in?
    @find_req =  RequestEvent.where("individual_id = :individual_id and company_id = :company_id",
        { individual_id: params[:individual_id], company_id: current_company.id }).first
 if @find_req.present?
    @request_approve =  RequestEvent.where(id: @find_req.id).first
 end


    end
    @resume = Document.where(profile_id: @profile.id)
    @profile_videos = Video.where(profile_id: @profile.id)
  end

  def portfolio_photo_uplaod
    portfolio_photo_params[:portfolio_photos].each do |photo|
      IndividualPhoto.create(portfolio_photo: photo, profile_id: portfolio_photo_params[:profile_id])
    end
    redirect_to individual_profile_path(current_individual)
  end

  def follow_individual
    if individual_signed_in? && @profile.individual_id != current_individual.id
      current_individual.follow(@profile)
    elsif company_signed_in?
      current_company.follow(@profile)
    end
    p 'followed'
    respond_to do |format|
      format.js { render :layout => false }
    end
  end

  def unfollow_individual
    if individual_signed_in? && @profile.individual_id != current_individual.id
      current_individual.stop_following(@profile)
    elsif company_signed_in?
      current_company.stop_following(@profile)
    end
    p 'unfollowed'
    respond_to do |format|

      format.js { render 'follow_individual.js.erb'}
    end
  end

  def resume_upload

    p params
    @resume = Document.create(resume_params)
    if @resume.save
      respond_to do |format|
      format.html { redirect_to individual_profile_path(current_individual), notice: 'Resume successfully uploaded.' }
      format.js { render :layout => false }
      end
    else
      respond_to do |format|
      format.html { redirect_to individual_profile_path(current_individual), alert: find_error(@resume) }
      format.js { render :layout => false }
      end
    end

  end

  def delete_portfolio
    p params
    @photo =  IndividualPhoto.where(id: params[:portfolio]).first
    if @photo.destroy
    respond_to do |format|
      format.html { redirect_to individual_profile_path(current_individual), notice: 'Image successfully deleted.' }
      format.js { render :layout => false }
    end
    end

  end


  private

  def find_profile
      @profile = Profile.where(individual_id: params[:individual_id]).first
    p @profile

  end

  def profile_params
    params.require(:profile).permit(:first_name, :last_name, :birth_date,
                                    :gender, :bio, :personal_website, :linkedin_profile, :description, :contact_no, :facebook_profile, :category_id, :individual_type,
                                    :mobile_no, :telephone_no, :tag_list, :age, :twitter_profile, :avatar, :work_experience, :education,
                                    :month_salary, :hr_salary,:area_interest => [],:looking_job => [], :weekday_availability => [], :weekend_availability => [])
  end

  def avatar_params
    params.require(:profile).permit(:avatar)
  end
  def portfolio_photo_params
    params.permit(:profile_id, :portfolio_photos => [])
  end

  def resume_params
    params.permit(:profile_id, :document, :name)
  end

end
