class OmniauthCallbacksController < Devise::OmniauthCallbacksController
  def all
    individual =  Individual.from_omniauth(request.env["omniauth.auth"])
    if individual.persisted?
      sign_in_and_redirect individual, notice: "Signed in"
    else
      session["devise.individual_attributes"] = individual.attributes
      redirect_to new_individual_registration_url
    end
  end
  alias_method :facebook, :all
end
