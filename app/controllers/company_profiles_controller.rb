class CompanyProfilesController < ApplicationController
  before_action :authenticate_company!, except: [:show, :find_profile, :follow_company, :unfollow_company]
  before_action :find_profile, only: [:show, :edit, :update, :destroy, :follow_company, :unfollow_company]
  # before_action :redirect_to_company_profile_create, :except => [:new, :create]
  # skip_before_action :redirect_to_profile_create, :only => [:new, :create, :destroy]
  before_action :redirect_to_profile_create, :except => [:new, :create, :destroy]
  def index
    @company_profiles = CompanyProfile.all
  end

  def new
    @company_profile = current_company.build_company_profile
    @categories = Category.all

  end

  def show
    @company_event = Event.where(company_id: params[:company_id], is_job: true).all
    @company_job = Event.where(company_id: params[:company_id]).all

    p params
    p @company_event
  end

  def create
    @company_profile = current_company.build_company_profile(profile_params)
    if @company_profile.save
      flash[:success] = "Profile saved"
      redirect_to  company_company_profile_path(current_company)
    else
      flash[:error] = "Error"
      render :new
    end
  end

  def logout

  end

  def update
    @company_profile.update(profile_params)
    redirect_to  company_company_profile_path(current_company)
  end

  def follow_company
    if individual_signed_in?
      current_individual.follow(@company_profile)
    elsif company_signed_in? && @company_profile.company_id != current_company.id
      current_company.follow(@company_profile)
    end
    p 'followed'
    respond_to do |format|
      format.js
    end
  end

  def unfollow_company
    if individual_signed_in?
      current_individual.stop_following(@company_profile)
    elsif company_signed_in? && @company_profile.company_id != current_company.id
      current_company.stop_following(@company_profile)
    end
    p 'unfollowed'
    respond_to do |format|

      format.js { render 'follow_company.js.erb'}
    end
  end

  def job_status
    @all_job = Event.where(:company_id => current_company.id)
p @pending_job
  end


  private

  def find_profile
    @company_profile = CompanyProfile.where(company_id: params[:company_id]).first
  end

  def profile_params
    params.require(:company_profile).permit(:company_name, :category_id, :contact_no, :formed_date, :type_company,
                                    :category, :bio, :personal_website, :linkedin_profile, :facebook_profile,
                                    :mobile_no, :telephone_no, :age, :twitter_profile, :company_avatar, :address1,
                                     :address2, :city, :state, :country, :pincode)
  end
end
