class VideosController < ApplicationController

  def new
    @video = Video.new
  end

  def create
    p 'ddddddddddddddddddddddddddddddddddddddddddddddddddddd'
    params[:profile_id] = params[:video][:profile_id]
    @event = Event.where(id: params[:id]).first
    @video = Video.create(video_params)
    if @video.save
      @message = 'Your Video has been saved'

    else
      @message = 'There was an error saving your video'
    end

    redirect_to individual_profile_path(individual_id:  current_individual.id)
  end

  private

  def video_params
    params.require(:video).permit(:link, :event_id, :profile_id)
  end

end
