class EventsController < ApplicationController
  include EventsHelper
  before_action :authenticate_company!, except: [:show, :index, :request_create, :follow_event, :unfollow_event, :jobs]
  before_action :individual_redirect_login, only: [:show]

  def index
    # @events = Event.all
    params[:filterrific][:with_category_id] = params[:with_category_id] unless params[:filterrific].nil?
    params[:filterrific][:with_place] = params[:with_place] unless params[:filterrific].nil?
    params[:filterrific][:with_type] = params[:with_type] unless params[:filterrific].nil?
    params[:filterrific][:with_classify] = params[:with_classify] unless params[:filterrific].nil?

    @filterrific = initialize_filterrific(
        Event,
        params[:filterrific],
        :select_options => {
             sorted_by: Event.options_for_sorted_by,
             with_category_id: Category.options_for_select

        }
    ) or return
    if params[:tag]
      @events = @filterrific.find.paginate(page: params[:page], per_page: 6).where(is_job: true).tagged_with(params[:tag]).where(job_status: 1)
    elsif params[:with_classify]
      @events = @filterrific.find.paginate(page: params[:page], per_page: 6).where(is_job: true).where(job_classify: params[:with_classify]).where(job_status: 1)
    else
      @events = @filterrific.find.paginate(page: params[:page], per_page: 6).where(is_job: true).where(job_status: 1)
    end
p params[:with_classify]
p params
p params[:filterrific]
p '11111111'
    p params[:page]
    p @events
    respond_to do |format|
      format.html
      format.js
    end

  end

  def jobs

    @filterrific_job = initialize_filterrific(
        Event,
        params[:filterrific],
        :select_options => {
            sorted_by: Event.options_for_sorted_by,
            with_category_id: Category.all

        }
    ) or return
    @events_job = @filterrific_job.find.page(params[:page]).where(is_job: true)



    p @events_job

    respond_to do |format|
      format.html
      format.js
    end


  end

  def new
    @event = current_company.events.build
    @categories = Category.all
  end

  def new_job
    @event = current_company.events.build
    @categories = Category.all
  end

  def create
    @event = current_company.events.build(event_params)
    if @event.save
      flash[:success] = "Profile saved"
      redirect_to company_event_job_pending_path(current_company, @event)
    else
      flash[:error] = "Error"
      render :new
    end
  end

  def job_pending

  end

  def update
    @event = Event.where(id: params[:id]).first

      if @event.update_attributes(event_params)
        redirect_to company_event_path(current_company, @event)


    end
  end

  def show
    p params
    p params[:inspect_event].blank?
      @applied_people = RequestEvent.where(:event_id => params[:id]).count
      @event = Event.where(id: params[:id]).first
      @video = Video.where(event_id: params[:id]).all
      if individual_signed_in?
      @request_event = RequestEvent.new
    if RequestEvent.exists?
      @find_req =  RequestEvent.where(individual_id: current_individual.id).first


    end
      @request_presence = RequestEvent.where( "event_id = :event_id and individual_id = :individual_id",
                                              { event_id: params[:id], individual_id: current_individual.id })

   end
      @count_request = RequestEvent.where(event_id: params[:id], status: "inprocess").count
      @request_people = RequestEvent.where(event_id: params[:id], status: "inprocess").pluck(:individual_id)
      @name = Profile.where(individual_id: @request_people)


     @interested_individual = Profile.where(category_id: @event.interests).all
      respond_to do |format|
        format.html
        format.js
      end

  end

  def event_update_info
    @event = Event.where(id: params[:id]).first
    if @event.update_attributes(event_info_params)
      redirect_to company_event_path(current_company, @event)
    end
  end


  def request_create
    p params
    @request_event = RequestEvent.create(request_params)
    if @request_event.save
      @message = 'Your Request has been processed, We will contact you shortly.'
    else
      @message = 'There was a problem creating a request, Please try again.'
    end
    respond_to do |format|
      format.js
    end
  end

  def follow_event
    @event = Event.where(id: params[:id]).first
    if individual_signed_in?
    current_individual.follow(@event)
    elsif company_signed_in? && @event.company_id != current_company.id
      current_company.follow(@event)
    end
    p 'followed'
    respond_to do |format|
      format.js
    end
  end

  def unfollow_event
    @event = Event.where(id: params[:id]).first
    if individual_signed_in?
      current_individual.stop_following(@event)
    elsif company_signed_in? && @event.company_id != current_company.id
      current_company.stop_following(@event)
    end
    p 'followed'
    respond_to do |format|

      format.js { render 'follow_event.js.erb'}
    end
  end

  private

  def event_params
    params.require(:event).permit(:name, :company_id, :category_id, :event_date, :event_time, :event_info, :is_job, :place, :event_avatar, :lattitude,
                                  :longitude,:find_name, :venue_name,:sub_locality, :locality,:tag_list, :type_job, :job_classify, :state, :country, :country_code, :postal_code,
                                  :full_address, :education, :gender, :experience, :person_req, :salary, interests: [])
  end

  def request_params
    params.require(:request_event).permit(:individual_id, :event_id, :company_id, :msg, :status)
  end

  def  individual_redirect_login
    unless company_signed_in?
      authenticate_individual!
    end
  end

end
