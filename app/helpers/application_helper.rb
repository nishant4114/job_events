module ApplicationHelper


p 'into the application helper'
  def resource_name
    :individual
  end

  def resource_class
    User
  end

  def resource
    @resource ||= Individual.new
  end

  def devise_mapping
    @devise_mapping ||= Devise.mappings[:individual]
  end
end
