module DeviseHelper
  def devise_error_messages!
    p 'this is method'
    return '' if resource.errors.empty?

    messages = resource.errors.full_messages.map { |msg| content_tag(:li, msg) }.join
    html = <<-HTML
    <script>
        $.growl.error({ message: "#{messages}" });
        </script>

    HTML

    html.html_safe
  end
end