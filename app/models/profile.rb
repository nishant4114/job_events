class Profile < ActiveRecord::Base
  belongs_to :individual
  belongs_to :category
  has_many :individual_photos
  has_many :videos
  has_one :document
  accepts_nested_attributes_for :individual_photos
  # enum individual_type: { professional: 0, jobseeker: 1, student: 2}
  acts_as_taggable_on :tags
  acts_as_followable
  has_attached_file :avatar, :styles => { :small => "150x150>" },
                    :s3_protocol => :https,
                    :url => ':s3_domain_url',
                    :path => '/avatars/:id/:style_:basename.:extension'


  # validates_attachment_presence :avatar, :message => 'Please select an image to upload'
  validates_attachment_size :avatar, :less_than => 2.megabytes, :message => 'Please select an image less than 2 MB'
   validates_attachment_content_type :avatar, :content_type => ['image/jpeg', 'image/png', 'image/jpg'], :message => 'Please upload image only'

  filterrific :default_filter_params => { :sorted_by => 'created_at_desc' },
              :available_filters => %w[
                sorted_by
                search_query
                with_category_id

                with_type

              ]

  # default for will_paginate
  self.per_page = 9

  # scope :with_place, lambda { |place|
  #                    where( locality: place )
  #
  #                  }
  # scope :with_classify, lambda { |classify|
  #                       where( job_classify: classify )
  #
  #                     }
  scope :with_type, lambda { |type|
                    where(individual_type: type)
                  }

  scope :with_category_id, lambda { |category_ids|
                           where(category_id: [*category_ids])
                         }

  scope :search_query, lambda { |query|

                       where("first_name LIKE ?", "%#{query}%")
                     }

  scope :sorted_by, lambda { |sort_option|
                    # extract the sort direction from the param value.
                    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
                    case sort_option.to_s
                      when /^created_at_/
                        # Simple sort on the created_at column.
                        # Make sure to include the table name to avoid ambiguous column names.
                        # Joining on other tables is quite common in Filterrific, and almost
                        # every ActiveRecord table has a 'created_at' column.
                        order("profiles.created_at #{ direction }")
                      when /^name_/
                        # Simple sort on the name colums
                        order("LOWER(students.last_name) #{ direction }, LOWER(students.first_name) #{ direction }")
                      when /^country_name_/
                        # This sorts by a student's country name, so we need to include
                        # the country. We can't use JOIN since not all students might have
                        # a country.
                        order("LOWER(countries.name) #{ direction }").includes(:country)
                      else
                        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
                    end
                  }

  def self.options_for_sorted_by
    [
        ['Name (a-z)', 'name_asc'],
        ['Registration date (newest first)', 'created_at_desc'],
        ['Registration date (oldest first)', 'created_at_asc'],
        ['Country (a-z)', 'country_name_asc']
    ]
  end

end
                                                                                 