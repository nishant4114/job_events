class RequestEvent < ActiveRecord::Base
  belongs_to :event
  belongs_to :individual
  belongs_to :company
  enum status: { inprocess: 0, onhold: 1, approved: 2, rejected: 3 }
end
