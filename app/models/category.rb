class Category < ActiveRecord::Base
  has_many :company_profiles
  has_many :profiles
  has_many :events

  def self.options_for_select
    order('LOWER(name)').map { |e| [e.name, e.id] }
  end

end
