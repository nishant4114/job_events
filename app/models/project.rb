class Project < ActiveRecord::Base
  belongs_to :profile
  belongs_to :company_profile
  belongs_to :category

  enum type_project: [ :software, :music, :graphics ]
end
