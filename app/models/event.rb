class Event < ActiveRecord::Base

  include PublicActivity::Model
  tracked owner: ->(controller, model) { controller && controller &&  controller.individual_signed_in? ? controller.current_individual : controller.current_company }
  # self.current_individual.id
  serialize :interests
  belongs_to :company
  belongs_to :category
  has_many :videos
  has_many :request_events
  has_many :individuals, through: :request_events
  accepts_nested_attributes_for :request_events
  acts_as_taggable_on :tags
  acts_as_followable

  enum job_status: {pending: 0, approved: 1, rejected: 2, closed: 3}


  has_attached_file :event_avatar, :styles => { :small => "150x150>" },
                    :url  => "/assets/products/:id/:style/:basename.:extension",
                    :path => ":rails_root/public/assets/products/:id/:style/:basename.:extension"

  #validates_attachment_presence :event_avatar
  validates_attachment_size :event_avatar, :less_than => 2.megabytes
  validates_attachment_content_type :event_avatar, :content_type => ['image/jpeg', 'image/png']

  filterrific :default_filter_params => { :sorted_by => 'created_at_desc' },
              :available_filters => %w[
                sorted_by
                search_query
                with_category_id
                with_place
                with_type
                with_classify
              ]

  # default for will_paginate
   self.per_page = 9

  scope :with_place, lambda { |place|
                     where( locality: place )

                   }
  scope :with_classify, lambda { |classify|
                     where( job_classify: classify )

                   }
  scope :with_type, lambda { |type|
                           where(type_job: type)
                  }

  scope :with_category_id, lambda { |category_ids|
                          where(category_id: [*category_ids])
                        }

  scope :search_query, lambda { |query|

                       where("name LIKE ?", "%#{query}%")
                     }

  scope :sorted_by, lambda { |sort_option|
                    # extract the sort direction from the param value.
                    direction = (sort_option =~ /desc$/) ? 'desc' : 'asc'
                    case sort_option.to_s
                      when /^created_at_/
                        # Simple sort on the created_at column.
                        # Make sure to include the table name to avoid ambiguous column names.
                        # Joining on other tables is quite common in Filterrific, and almost
                        # every ActiveRecord table has a 'created_at' column.
                        order("events.created_at #{ direction }")
                      when /^name_/
                        # Simple sort on the name colums
                        order("LOWER(students.last_name) #{ direction }, LOWER(students.first_name) #{ direction }")
                      when /^country_name_/
                        # This sorts by a student's country name, so we need to include
                        # the country. We can't use JOIN since not all students might have
                        # a country.
                        order("LOWER(countries.name) #{ direction }").includes(:country)
                      else
                        raise(ArgumentError, "Invalid sort option: #{ sort_option.inspect }")
                    end
                  }

  def self.options_for_sorted_by
    [
        ['Name (a-z)', 'name_asc'],
        ['Registration date (newest first)', 'created_at_desc'],
        ['Registration date (oldest first)', 'created_at_asc'],
        ['Country (a-z)', 'country_name_asc']
    ]
  end

end
