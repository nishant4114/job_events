class Document < ActiveRecord::Base
  belongs_to :profile

  has_attached_file :document

  validates_presence_of :document, :message => 'no document found'
  validates_attachment_size :document, :less_than => 2.megabytes, :message => 'Please select a document less than 2 MB'
  validates_attachment_content_type :document, :content_type => ["application/pdf","application/vnd.ms-excel",
                                                                 "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                                                                 "application/msword",
                                                                 "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
                                                                 "text/plain"], :message => 'Please upload a document only',
                                                                :s3_protocol => :https,
                                                                :url => ':s3_domain_url',
                                                                :path => '/documents/:id/:style_:basename.:extension'
end
