class CompanyProfile < ActiveRecord::Base
  belongs_to :company
  acts_as_followable

  has_attached_file :company_avatar, :styles => { :small => "150x150>" },
                    :s3_protocol => :https,
                    :url => ':s3_domain_url',
                    :path => '/company_avatar/:id/:style_:basename.:extension'

    # validates_attachment_presence :company_avatar
   validates_attachment_size :company_avatar, :less_than => 2.megabytes
   validates_attachment_content_type :company_avatar, :content_type => ['image/jpeg', 'image/png']

end
