class Company < ActiveRecord::Base
  has_many :events
  has_many :jobs
  has_one :company_profile
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable
  acts_as_followable
  acts_as_follower
end
