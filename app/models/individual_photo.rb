class IndividualPhoto < ActiveRecord::Base
  belongs_to :profile

  has_attached_file :portfolio_photo, :styles => { :medium => "300x300>" }
  validates_attachment_content_type :portfolio_photo, :content_type => ["image/jpg", "image/jpeg", "image/png", "image/gif"],
                                                      :s3_protocol => :https,
                                                      :url => ':s3_domain_url',
                                                      :path => '/company_avatar/:id/:style_:basename.:extension'
end
