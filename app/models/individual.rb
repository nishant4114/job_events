class Individual < ActiveRecord::Base
  has_one :profile
  has_many :request_events
  has_many :events, through: :request_events
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable, :omniauthable,
         :recoverable, :rememberable, :trackable, :validatable, :confirmable

  acts_as_follower


  def self.from_omniauth(auth)
    where(provider: auth.provider, uid: auth.uid).first_or_create do |individual|
      individual.email = auth["extra"]["raw_info"]["email"]
      individual.provider = auth.provider
      individual.uid = auth.uid
      individual.skip_confirmation!

    end
  end

  def self.new_with_session(params, session)
    if session["devise.individual_attributes"]
      new(session["devise.individual_attributes"], without_protection: true) do |individual|
        individual.attributes = params
        individual.valid?
      end

    else
      super
    end
    # super.tap do |individual|
    #   if data = session["devise.facebook_data"] && session["devise.facebook_data"]["extra"]["raw_info"]
    #     individual.email = data["email"] if individual.email.blank?
    #   end
    # end
  end

  def password_required?
    super && provider.blank?
  end
  def email_required?
    super && provider.blank?
  end

end
