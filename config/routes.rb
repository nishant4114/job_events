Rails.application.routes.draw do
  devise_for :companies
  root 'welcome#index'
  resources :activities, only: [:index]

  get '/about_us' => 'welcome#about_us'

  devise_scope :company do
  match 'companies/sign_out', to: 'devise/sessions#destroy', via: 'delete'
  end
  devise_scope :individual do
    match 'individuals/sign_out', to: 'devise/sessions#destroy', via: 'delete'
  end

  devise_for :individuals, controllers: {omniauth_callbacks: "omniauth_callbacks", registrations: "registrations"}
resources :profiles do
  get :autocomplete_tag_name, :on => :collection
end
  resources :individuals do
    resource :profile do
    member do
      post :follow_individual
      post :unfollow_individual
      post :portfolio_photo_uplaod
      post :resume_upload
      post :delete_portfolio
      post :delete_video
      post :update_profile
      end
    end
  end
  get 'tags/:tag' => 'events#index', as: :tag
  get 'classify/:with_classify' => 'events#index', as: :with_classify



  resources :companies do
    resource :company_profile do
      member do
        post :follow_company
        post :unfollow_company
      end
    end
    resources :events do
      member do
        post :follow_event
        post :unfollow_event

      end
      collection do
        get :new_job
      end
      get :job_pending
    end
    resources :company_profiles do
      collection do
        get :job_status
      end
    end

  end

  resources :events do
    collection do
      post 'request_create'
      get 'jobs'
    end
  end
resources :request_events
  resources :videos do
    collection do
      post :create
    end
  end

  resources :categories


resources :projects
  resources :queries do
    collection do
      post :create
    end
  end

resources :seekers

  resources :xviumer_profiles do
    collection do
    get :job_approval
    post :job_approve
      end
  end


  resources :job_feeds


  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"


  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
